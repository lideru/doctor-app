import request from '@/utils/request'

 
// 提交
export function userAppointment(data) {
  return request({
    'url': '/maincode/appointment/userAppointment',
    'method': 'post',
	'data' : data,
	})
}
// 用户查询自己的预约记录
export function getUserAppointment(param) {
  return request({
    'url': '/maincode/appointment/getUserAppointment',
    'method': 'get',
	'params' : param
  })
} 

// 用户查询自己评论预约记录
export function getUserCommentAppointment(param) {
  return request({
    'url': '/maincode/appointment/getUserCommentAppointment',
    'method': 'get',
	'params' : param
  })
} 


// 用户查询自己评论预约记录
export function getAppointment(id) {
  return request({
    'url': '/maincode/appointment/getAppointment/' + id,
    'method': 'get'
  })
} 


// 评价
export function userComment(data) {
  return request({
    'url': '/maincode/appointment/userComment',
    'method': 'post',
	'data' : data,
	})
}


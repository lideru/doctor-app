import request from '@/utils/request'

 
// 获取活动轮播
export function getActivityList(param) {
  return request({
    'url': '/maincode/activity/getActivityList',
    'method': 'get',
	'params' : param
  })
} 

// 获取活动轮播
export function getActivity(id) {
  return request({
    'url': '/maincode/activity/getActivity/' + id,
    'method': 'get'
  })
} 

// 应用全局配置
module.exports = {
  baseUrl: 'http://localhost:6680',
  // 应用信息
  appInfo: {
    // 应用名称
    name: "app",
    // 应用版本
    version: "1.1.0",
    // 应用logo
    logo: "/static/logo.png",
    // 官方网站
    site_url: "",
    // 政策协议
    agreements: [{
        title: "隐私政策",
        url: "https://blog.csdn.net/weixin_45730866?spm=1000.2115.3001.5343"
      },
      {
        title: "用户服务协议",
        url: "https://blog.csdn.net/weixin_45730866?spm=1000.2115.3001.5343"
      }
    ]
  }
}
